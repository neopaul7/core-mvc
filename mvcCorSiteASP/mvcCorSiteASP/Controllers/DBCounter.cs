﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Specialized;

namespace mvcCorSiteASP
{
    public class DBCounter : ICounter
    {
        public void IncreaseCounter()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConStr"].ConnectionString))
            {
                sqlConnection.Open();

                string cmdText = $"UPDATE dbo.MyTabCounter SET MyCounter = MyCounter + 1";
                using (var sqlCmd = new SqlCommand(cmdText, sqlConnection))
                {
                    sqlCmd.ExecuteScalar();
                }
            }
        }
    }
}
