﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

namespace mvcCorSiteASP
{
    public class FSCounter : ICounter
    {
        int tempNumber;
        public void IncreaseCounter()
        {
            // read from file to variable
            using (StreamReader reader = new StreamReader(ConfigurationManager.AppSettings.Get("FilePath")))
            {
                tempNumber = Int32.Parse(reader.ReadLine());
            }
            // write to file from variable
            using (StreamWriter writer = new StreamWriter(ConfigurationManager.AppSettings.Get("FilePath")))
            {
                writer.WriteLine(++tempNumber);
            }
        }
    }
}
