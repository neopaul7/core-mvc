﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mvcCorSiteASP
{
    public class MyCounterMidWar
    {
        private readonly RequestDelegate nextMidleWare;
        ICounter counter;

        public MyCounterMidWar(RequestDelegate nextMidleWare, ICounter counter)
        {
            this.counter = counter;
            this.nextMidleWare = nextMidleWare;
        }


        public async Task InvokeAsync(HttpContext context)
        {
            counter.IncreaseCounter();
            await nextMidleWare.Invoke(context);
        }
    }
}
