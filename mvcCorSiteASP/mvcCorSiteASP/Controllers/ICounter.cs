﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mvcCorSiteASP
{
    public interface ICounter
    {
        void IncreaseCounter();
    }
}
